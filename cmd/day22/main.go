package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day22"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("X: %d\n", day22.X(input))
	fmt.Printf("Y: %d\n", day22.Y(input))
}
