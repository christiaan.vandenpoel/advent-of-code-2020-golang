package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day23"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("X: %d\n", day23.X(input))
	fmt.Printf("Y: %d\n", day23.Y(input))
}
