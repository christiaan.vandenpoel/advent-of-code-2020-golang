package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day04"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("CountValidPassports:         %d\n", day04.CountValidPassports(input))
	fmt.Printf("CountValidPassportsPartTwo:  %d\n", day04.CountValidPassportsPartTwo(input))
}
