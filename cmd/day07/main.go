package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day07"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("HandyHaversacksBagsForShinyGold:     %d\n", day07.HandyHaversacksBagsForShinyGold(input))
	fmt.Printf("HandyHaversacksCountBagsInShinyGold: %d\n", day07.HandyHaversacksCountBagsInShinyGold(input))
}
