package main

import (
	"fmt"

	"advent-of-code-2020/pkg/day17"
	"advent-of-code-2020/pkg/utils"
)

func main() {
	input := utils.ReadStringFromFile("input.txt")

	fmt.Printf("ConwayCubesAfter6Cycles: %d\n", day17.ConwayCubesAfter6Cycles(input))
	fmt.Printf("Y: %d\n", day17.Y(input))
}
