package utils

type TreeMap map[int]map[int]bool

func NewTreeMap() TreeMap {
	return make(TreeMap)
}

func (tree TreeMap) HasTree(x, y int) bool {
	return tree[x][y]
}

func (tree TreeMap) Mark(x, y int, value rune) {
	if tree[x] == nil {
		tree[x] = map[int]bool{}
	}
	tree[x][y] = value == '#'
}
