package utils

import "fmt"

type Location struct {
	X, Y int
}

func NewLocation(x, y int) Location {
	return Location{X: x, Y: y}
}

func (location Location) Equals(other Location) bool {
	return location.X == other.X && location.Y == other.Y
}

func (location Location) String() string {
	return fmt.Sprintf("Location{x: %d, y: %d}", location.X, location.Y)
}
