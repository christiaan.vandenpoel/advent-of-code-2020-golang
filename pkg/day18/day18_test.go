package day18_test

import (
	"testing"

	"advent-of-code-2020/pkg/day18"
)

func TestOperationOrderSumOfLineResults(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"", 0},
	}

	for _, test := range tests {
		actual := day18.OperationOrderSumOfLineResults(test.in)
		if actual != test.out {
			t.Errorf("OperationOrderSumOfLineResults(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestY(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"", 0},
	}

	for _, test := range tests {
		actual := day18.Y(test.in)
		if actual != test.out {
			t.Errorf("Y(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
