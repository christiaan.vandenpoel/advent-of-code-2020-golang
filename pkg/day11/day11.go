package day11

import (
	"fmt"
	"reflect"
	"strings"
)

func SeatingSystemTotalOccupiedSeatsDirectlyAdjacent(input string) int {
	seets := parseSeets(input)

	currentSeets := seets
	newSeets := processSeets(currentSeets)
	for !reflect.DeepEqual(newSeets, currentSeets) {
		currentSeets = newSeets
		newSeets = processSeets(currentSeets)
	}
	return countOccupiedSeets(newSeets)
}

func parseSeets(input string) (seets map[int]map[int]string) {
	seets = make(map[int]map[int]string)

	lines := strings.Split(input, "\n")
	for y, line := range lines {
		for x, char := range line {
			if _, ok := seets[x]; !ok {
				seets[x] = make(map[int]string)
			}
			seets[x][y] = string(char)
		}
	}
	return
}

func printSeets(seets map[int]map[int]string, title string) {
	fmt.Printf("%s\n", title)
	width := len(seets)
	height := len(seets[0])
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			fmt.Print(seets[x][y])
		}
		fmt.Println()
	}
	fmt.Println()
}

func setFloor(seets map[int]map[int]string, x, y int) {
	set(seets, x, y, ".")
}

func setEmpty(seets map[int]map[int]string, x, y int) {
	set(seets, x, y, "L")
}

func setOccupied(seets map[int]map[int]string, x, y int) {
	set(seets, x, y, "#")
}

func occupedAdjacent(seets map[int]map[int]string, x, y int) int {
	occupied := 0

	for xx := x - 1; xx <= x+1; xx++ {
		for yy := y - 1; yy <= y+1; yy++ {
			if x == xx && y == yy {
				continue
			}

			if _, ok := seets[xx]; ok {
				if seet, ok := seets[xx][yy]; ok {
					if seet == "#" {
						occupied++
					}
				}
			}
		}
	}

	return occupied
}

func set(seets map[int]map[int]string, x, y int, val string) {
	if _, ok := seets[x]; !ok {
		seets[x] = make(map[int]string)
	}
	seets[x][y] = val
}

func processSeets(currentSeets map[int]map[int]string) map[int]map[int]string {
	newSeets := make(map[int]map[int]string)
	width := len(currentSeets)
	height := len(currentSeets[0])

	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			seet := currentSeets[x][y]
			switch seet {
			case ".":
				setFloor(newSeets, x, y)
			case "L":
				if occupedAdjacent(currentSeets, x, y) == 0 {
					setOccupied(newSeets, x, y)
				} else {
					setEmpty(newSeets, x, y)
				}
			case "#":
				if occupedAdjacent(currentSeets, x, y) >= 4 {
					setEmpty(newSeets, x, y)
				} else {
					setOccupied(newSeets, x, y)
				}
			}
		}
	}

	return newSeets
}

func countOccupiedSeets(seets map[int]map[int]string) int {
	total := 0
	width := len(seets)
	height := len(seets[0])
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			if seets[x][y] == "#" {
				total++
			}
		}
	}
	return total
}

func occupedLongRange(seets map[int]map[int]string, x, y int) int {
	occupied := 0
	progress := 0
	width := len(seets)

	const (
		LEFT = 1 << iota
		TOP_LEFT
		TOP
		TOP_RIGHT
		RIGHT
		BOTTOM_RIGHT
		BOTTOM
		BOTTOM_LEFT
	)

	for idx := 1; idx <= width && progress != 0xFF; idx++ {
		// left
		check(x-idx, y, seets, LEFT, &progress, &occupied)
		// top-left
		check(x-idx, y+idx, seets, TOP_LEFT, &progress, &occupied)
		// top
		check(x, y+idx, seets, TOP, &progress, &occupied)
		// top-right
		check(x+idx, y+idx, seets, TOP_RIGHT, &progress, &occupied)
		// right
		check(x+idx, y, seets, RIGHT, &progress, &occupied)
		// bottom-right
		check(x+idx, y-idx, seets, BOTTOM_RIGHT, &progress, &occupied)
		// bottom
		check(x, y-idx, seets, BOTTOM, &progress, &occupied)
		// bottom-left
		check(x-idx, y-idx, seets, BOTTOM_LEFT, &progress, &occupied)
	}

	return occupied
}

func check(x, y int, seets map[int]map[int]string, bit int, progress *int, occupied *int) {
	v, ok := seets[x][y]
	if *progress&bit == 0 {
		if ok {
			switch v {
			case "#":
				*occupied++
				*progress |= bit
			case "L":
				*progress |= bit
			}
		} else {
			*progress |= bit
		}
	}
}

func processSeetsLongRange(currentSeets map[int]map[int]string) map[int]map[int]string {
	newSeets := make(map[int]map[int]string)
	width := len(currentSeets)
	height := len(currentSeets[0])

	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			seet := currentSeets[x][y]
			switch seet {
			case ".":
				setFloor(newSeets, x, y)
			case "L":
				if occupedLongRange(currentSeets, x, y) == 0 {
					setOccupied(newSeets, x, y)
				} else {
					setEmpty(newSeets, x, y)
				}
			case "#":
				if occupedLongRange(currentSeets, x, y) >= 5 {
					setEmpty(newSeets, x, y)
				} else {
					setOccupied(newSeets, x, y)
				}
			}
		}
	}

	return newSeets
}

func SeatingSystemTotalOccupiedSeatsVisiblyAdjacent(input string) int {
	seets := parseSeets(input)

	currentSeets := seets

	newSeets := processSeetsLongRange(currentSeets)
	for !reflect.DeepEqual(newSeets, currentSeets) {
		currentSeets = newSeets

		newSeets = processSeetsLongRange(currentSeets)
	}
	return countOccupiedSeets(newSeets)
}
