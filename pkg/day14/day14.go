package day14

import (
	"advent-of-code-2020/pkg/utils"
	"fmt"
	"math"
	"regexp"
	"strings"
)

func DockingDataMemorySum(input string) int {
	lines := strings.Split(input, "\n")

	maskRegex := regexp.MustCompile(`mask = ([01X]{36})`)
	memoryRegex := regexp.MustCompile(`mem\[(\d+)\] = (\d+)`)
	orMask := 0
	andMask := 0
	memory := make(map[int]int)

	for _, line := range lines {
		if maskRegex.MatchString(line) {
			maskPattern := maskRegex.FindStringSubmatch(line)[1]
			orMask, andMask = MaskToBitmaps(maskPattern)
		} else if memoryRegex.MatchString(line) {
			matches := memoryRegex.FindStringSubmatch(line)[1:]
			value := utils.MustInt(matches[1])
			address := utils.MustInt(matches[0])

			value |= orMask
			value &= andMask
			memory[address] = value
		} else {
			panic(fmt.Sprintf("String not matched: %q\n", line))
		}
	}

	sum := 0

	for _, value := range memory {
		sum += value
	}

	return sum
}

func MaskToBitmaps(mask string) (or, and int) {

	and = 0xFFFFFFFFF
	or = 0

	for idx := 0; idx < 36; idx++ {
		char := mask[len(mask)-idx-1]
		if char == '0' {
			and = and &^ (1 << idx)
		}
		if char == '1' {
			or = or | 1<<idx
		}
	}

	return
}

type maskType struct {
	or     int
	floats []int
}

func NewMask(mask string) maskType {
	or := 0
	floats := []int{}

	for idx := 0; idx < 36; idx++ {
		char := mask[len(mask)-idx-1]
		if char == '1' {
			or = or | 1<<idx
		}
		if char == 'X' {
			floats = append(floats, 1<<idx)
		}
	}

	return maskType{or: or, floats: floats}
}

func MemoryAddresses(mask maskType, address int) (addresses []int) {
	addresses = []int{}

	nrFloats := len(mask.floats)
	combinations := int(math.Pow(2, float64(nrFloats)))

	for combination := 0; combination < combinations; combination++ {
		newAddress := address | mask.or

		for bitIndex := 0; bitIndex < nrFloats; bitIndex++ {
			bit := (combination & (1 << bitIndex)) >> (bitIndex)
			if bit == 1 {
				newAddress |= mask.floats[bitIndex]
			} else {
				newAddress &^= mask.floats[bitIndex]
			}
		}
		addresses = append(addresses, newAddress)
	}

	return
}

func DockingDataMemorySumVersion2(input string) int {
	lines := strings.Split(input, "\n")

	maskRegex := regexp.MustCompile(`mask = ([01X]{36})`)
	memoryRegex := regexp.MustCompile(`mem\[(\d+)\] = (\d+)`)
	var mask maskType
	memory := make(map[int]int)

	for _, line := range lines {
		if maskRegex.MatchString(line) {
			maskPattern := maskRegex.FindStringSubmatch(line)[1]

			mask = NewMask(maskPattern)

		} else if memoryRegex.MatchString(line) {
			matches := memoryRegex.FindStringSubmatch(line)[1:]

			value := utils.MustInt(matches[1])
			address := utils.MustInt(matches[0])

			for _, add := range MemoryAddresses(mask, address) {
				memory[add] = value
			}

		} else {
			panic(fmt.Sprintf("String not matched: %q\n", line))
		}
	}

	sum := 0

	for _, value := range memory {
		sum += value
	}

	return sum
}
