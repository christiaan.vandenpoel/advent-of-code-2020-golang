package day10

import (
	"advent-of-code-2020/pkg/utils"
	"math"
	"sort"
	"strings"
)

type adapterType int

func AdapterArrayJoltDifference(input string) int {
	bagOfAdapters := []int{}

	for _, adapter := range strings.Split(input, "\n") {
		bagOfAdapters = append(bagOfAdapters, utils.MustInt(adapter))
	}
	sort.Ints(bagOfAdapters)
	deviceAdapter := bagOfAdapters[len(bagOfAdapters)-1] + 3
	bagOfAdapters = append(bagOfAdapters, deviceAdapter)

	differences := make(map[int]int)

	for idx, adapter := range bagOfAdapters {
		if idx == 0 {
			differences[adapter]++
		} else {
			differences[adapter-bagOfAdapters[idx-1]]++
		}
	}

	return differences[1] * differences[3]
}

func AdapterArrayPossibleCombinations(input string) int {
	bagOfAdapters := []int{}

	for _, adapter := range strings.Split(input, "\n") {
		bagOfAdapters = append(bagOfAdapters, utils.MustInt(adapter))
	}
	bagOfAdapters = append(bagOfAdapters, 0)
	sort.Ints(bagOfAdapters)
	deviceAdapter := bagOfAdapters[len(bagOfAdapters)-1] + 3
	bagOfAdapters = append(bagOfAdapters, deviceAdapter)

	// investigate our adapters
	consecutiveDifferencesOfOnes := make(map[int]int)
	previousDifference := 0
	previousAdapter := bagOfAdapters[0]
	currentCount := 0
	for i := 1; i < len(bagOfAdapters)-1; i++ {
		adapter := bagOfAdapters[i]

		difference := adapter - previousAdapter

		if difference != previousDifference {
			if previousDifference == 1 {
				consecutiveDifferencesOfOnes[currentCount+1]++
			}
			currentCount = 1
		} else {
			currentCount++
		}
		previousAdapter = adapter
		previousDifference = difference
	}
	if previousDifference == 1 {
		consecutiveDifferencesOfOnes[currentCount+1]++
	}
	return int(
		math.Pow(4, float64(consecutiveDifferencesOfOnes[4])) *
			math.Pow(2, float64(consecutiveDifferencesOfOnes[3])) *
			math.Pow(7, float64(consecutiveDifferencesOfOnes[5])))

}
