package day02_test

import (
	"testing"

	"advent-of-code-2020/pkg/day02"
)

func TestCountValidPasswords(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{`1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc`, 2},
	}

	for _, test := range tests {
		actual := day02.CountValidPasswords(test.in)
		if actual != test.out {
			t.Errorf("X(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestCountValidPasswordsSecondPart(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{`1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc`, 1},
	}

	for _, test := range tests {
		actual := day02.CountValidPasswordsSecondPart(test.in)
		if actual != test.out {
			t.Errorf("Y(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
