package day03

import (
	"advent-of-code-2020/pkg/utils"
	"strings"
)

func TobogganTrajectorySingleSlope(input string) int {
	treeMap := readTreeMap(input)

	treeCount := traverseTrees(treeMap, 3, 1)

	return treeCount
}

func TobogganTrajectoryMultipleSlopes(input string) int {
	slopes := []struct {
		x int
		y int
	}{
		{1, 1},
		{3, 1},
		{5, 1},
		{7, 1},
		{1, 2},
	}

	treeCount := 1
	treeMap := readTreeMap(input)

	for _, slope := range slopes {
		treeCount *= traverseTrees(treeMap, slope.x, slope.y)
	}

	return treeCount
}

func readTreeMap(input string) utils.TreeMap {
	treeMap := utils.NewTreeMap()

	for y, line := range strings.Split(input, "\n") {
		for x, char := range line {
			treeMap.Mark(x, y, char)
		}
	}

	return treeMap
}

func traverseTrees(treeMap utils.TreeMap, xOffset int, yOffset int) int {
	width := len(treeMap)
	height := len(treeMap[0])

	treeCount := 0

	currentX := 0
	currentY := 0

	for currentY < height {
		if treeMap.HasTree(currentX%width, currentY) {
			treeCount++
		}
		currentX += xOffset
		currentY += yOffset
	}
	return treeCount
}
