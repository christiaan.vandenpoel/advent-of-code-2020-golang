package day12

import (
	"advent-of-code-2020/pkg/utils"
	"fmt"
	"strings"
)

func RainRiskManhattanDistance(input string) int {
	ship := NewShip()
	ship.Advance(input)
	return ship.ManhattanDistance()
}
func RainRiskManhattanDistanceWithWaypoint(input string) int {
	ship := NewShip()
	ship.AdvanceWithWaypoint(input)
	return ship.ManhattanDistance()
}

type ship struct {
	direction Direction
	location  utils.Location
	waypoint  utils.Location
}

//go:generate stringer -type=Direction
type Direction int

func NewShip() *ship {
	return &ship{
		direction: East,
		location:  utils.NewLocation(0, 0),
		waypoint:  utils.NewLocation(10, 1),
	}
}

const (
	East Direction = iota
	South
	West
	North
)

func (ship *ship) CurrentDirection() Direction {
	return ship.direction
}

func (ship *ship) CurrentLocation() utils.Location {
	return ship.location
}
func (ship *ship) WaypointLocation() utils.Location {
	return ship.waypoint
}

func (ship *ship) Advance(input string) {
	movements := strings.Split(input, "\n")

	for _, movement := range movements {
		steps := utils.MustInt(movement[1:])
		action := movement[0]
		switch action {
		case 'F':
			ship.forward(steps)
		case 'N':
			ship.north(steps)
		case 'E':
			ship.east(steps)
		case 'S':
			ship.south(steps)
		case 'W':
			ship.west(steps)
		case 'R':
			ship.moveRight(steps)
		case 'L':
			ship.moveLeft(steps)
		default:
			panic(fmt.Errorf("Advance: unknown movement: %q", movement[0]))
		}
	}
}

func (ship *ship) AdvanceWithWaypoint(input string) {
	movements := strings.Split(input, "\n")

	for _, movement := range movements {
		steps := utils.MustInt(movement[1:])
		action := movement[0]
		switch action {
		case 'F':
			ship.forwardWithWaypoint(steps)
		case 'N':
			ship.northWaypoint(steps)
		case 'E':
			ship.eastWaypoint(steps)
		case 'S':
			ship.southWaypoint(steps)
		case 'W':
			ship.westWaypoint(steps)
		case 'R':
			ship.moveRightWaypoint(steps)
		case 'L':
			ship.moveLeftWaypoint(steps)
		default:
			panic(fmt.Errorf("Advance: unknown movement: %q", movement[0]))
		}
	}
}

func (ship *ship) ManhattanDistance() int {
	return abs(ship.location.X) + abs(ship.location.Y)
}

func abs(in int) int {
	if in < 0 {
		return -1 * in
	}
	return in
}

func (ship *ship) forward(steps int) {
	switch ship.direction {
	case East:
		ship.location.X += steps
	case South:
		ship.location.Y -= steps
	case North:
		ship.location.Y += steps
	case West:
		ship.location.X -= steps
	default:
		panic(fmt.Errorf("forward: unsupported direction: %v", ship.direction))
	}
}

func (ship *ship) forwardWithWaypoint(steps int) {
	ship.location.X += steps * ship.waypoint.X
	ship.location.Y += steps * ship.waypoint.Y
}

func (ship *ship) south(steps int) {
	ship.location.Y -= steps
}

func (ship *ship) north(steps int) {
	ship.location.Y += steps
}

func (ship *ship) east(steps int) {
	ship.location.X += steps
}

func (ship *ship) west(steps int) {
	ship.location.X -= steps
}

func (ship *ship) northWaypoint(steps int) {
	ship.waypoint.Y += steps
}

func (ship *ship) eastWaypoint(steps int) {
	ship.waypoint.X += steps
}

func (ship *ship) southWaypoint(steps int) {
	ship.waypoint.Y -= steps
}

func (ship *ship) westWaypoint(steps int) {
	ship.waypoint.X -= steps
}

func (ship *ship) moveLeftWaypoint(steps int) {
	times := steps / 90
	for cnt := 0; cnt < times; cnt++ {
		wp := utils.NewLocation(-1*ship.waypoint.Y, ship.waypoint.X)
		ship.waypoint = wp
	}
}

func (ship *ship) moveRightWaypoint(steps int) {
	times := steps / 90
	for cnt := 0; cnt < times; cnt++ {
		wp := utils.NewLocation(ship.waypoint.Y, -1*ship.waypoint.X)
		ship.waypoint = wp
	}
}

func (ship *ship) moveRight(steps int) {
	directionMap := map[Direction]map[int]Direction{
		East: {
			90:  South,
			180: West,
			270: North},
		South: {
			90:  West,
			180: North,
			270: East},
		North: {
			90:  East,
			180: South,
			270: West},
		West: {
			90:  North,
			180: East,
			270: South,
		},
	}
	ship.direction = directionMap[ship.direction][steps]
}

func (ship *ship) moveLeft(steps int) {
	directionMap := map[Direction]map[int]Direction{
		West: {
			90:  South,
			180: East,
			270: North},
		South: {
			90:  East,
			180: North,
			270: West},
		East: {
			90:  North,
			180: West,
			270: South},
		North: {
			90:  West,
			180: South,
			270: East,
		},
	}
	ship.direction = directionMap[ship.direction][steps]
}
