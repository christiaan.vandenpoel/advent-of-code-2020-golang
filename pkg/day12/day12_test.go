package day12_test

import (
	"testing"

	"advent-of-code-2020/pkg/day12"
	"advent-of-code-2020/pkg/utils"
)

func TestRainRiskManhattanDistance(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{`F10
N3
F7
R90
F11`, 25},
	}

	for _, test := range tests {
		actual := day12.RainRiskManhattanDistance(test.in)
		if actual != test.out {
			t.Errorf("RainRiskManhattanDistance(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestRainRiskManhattanDistanceWithWaypoint(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{`F10
N3
F7
R90
F11`, 286},
	}

	for _, test := range tests {
		actual := day12.RainRiskManhattanDistanceWithWaypoint(test.in)
		if actual != test.out {
			t.Errorf("RainRiskManhattanDistanceWithWaypoint(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestShipCreation(t *testing.T) {
	ship := day12.NewShip()

	if ship.CurrentDirection() != day12.East {
		t.Errorf("ship.CurrentDirection() => %v, want %v", ship.CurrentDirection(), day12.East)
	}

	expected := utils.NewLocation(0, 0)
	if !ship.CurrentLocation().Equals(expected) {
		t.Errorf("ship.CurrentLocation() => %v, want %v", ship.CurrentLocation(), expected)
	}
}

func TestShipNavigation(t *testing.T) {
	tests := []struct {
		in        string
		loc       utils.Location
		direction day12.Direction
	}{
		{`F10`, utils.NewLocation(10, 0), day12.East},
		{`F10
N3`, utils.NewLocation(10, 3), day12.East},
		{`F10
N3
F7`, utils.NewLocation(17, 3), day12.East},
		{`F10
N3
F7
R90`, utils.NewLocation(17, 3), day12.South},
		{`F10
N3
F7
R90
F11`, utils.NewLocation(17, -8), day12.South},
		{`E10`, utils.NewLocation(10, 0), day12.East},
		{`S12`, utils.NewLocation(0, -12), day12.East},
		{`R90
R90`, utils.NewLocation(0, 0), day12.West},
		{`R90
R90
L90`, utils.NewLocation(0, 0), day12.South},
		{`R90
L180`, utils.NewLocation(0, 0), day12.North},
		{`L90
F14`, utils.NewLocation(0, 14), day12.North},
		{`L90
F14
R90`, utils.NewLocation(0, 14), day12.East},
		{`L90
L180`, utils.NewLocation(0, 0), day12.South},
		{`L180`, utils.NewLocation(0, 0), day12.West},
		{`L180
F18`, utils.NewLocation(-18, 0), day12.West},
		{`L180
R180`, utils.NewLocation(0, 0), day12.East},
		{`R180`, utils.NewLocation(0, 0), day12.West},
		{`R90
L270`, utils.NewLocation(0, 0), day12.West},
		{`R180
L180`, utils.NewLocation(0, 0), day12.East},
		{`R180
R90`, utils.NewLocation(0, 0), day12.North},
		{`L90
L90`, utils.NewLocation(0, 0), day12.West},
		{`R90
L90`, utils.NewLocation(0, 0), day12.East},
		{`L180
R270`, utils.NewLocation(0, 0), day12.South},
		{`R90
R180`, utils.NewLocation(0, 0), day12.North},
		{`R270`, utils.NewLocation(0, 0), day12.North},
		{`R270
L270`, utils.NewLocation(0, 0), day12.East},
		{`L270`, utils.NewLocation(0, 0), day12.South},
		{`L180
L270`, utils.NewLocation(0, 0), day12.North},
		{`R90
R270`, utils.NewLocation(0, 0), day12.East},
		{`L90
R180`, utils.NewLocation(0, 0), day12.South},
		{`L90
R270`, utils.NewLocation(0, 0), day12.West},
	}

	for _, test := range tests {
		ship := day12.NewShip()
		ship.Advance(test.in)
		actualLoc := ship.CurrentLocation()
		if !actualLoc.Equals(test.loc) || ship.CurrentDirection() != test.direction {
			t.Errorf("Advance(%q) => (%s, %v), want (%s, %v)", test.in, actualLoc, ship.CurrentDirection(), test.loc, test.direction)
		}
	}
}

func TestShipNavigationWithWaypoint(t *testing.T) {
	tests := []struct {
		in       string
		loc      utils.Location
		waypoint utils.Location
	}{
		{`F10`, utils.NewLocation(10*10, 10*1), utils.NewLocation(10, 1)},
		{`F10
N3`, utils.NewLocation(100, 10), utils.NewLocation(10, 4)},
		{`F10
N3
F7`, utils.NewLocation(170, 38), utils.NewLocation(10, 4)},
		{`F10
N3
F7
R90`, utils.NewLocation(170, 38), utils.NewLocation(4, -10)},
		{`F10
N3
F7
R90
F11`, utils.NewLocation(214, -72), utils.NewLocation(4, -10)},
		{`E10`, utils.NewLocation(0, 0), utils.NewLocation(20, 1)},
		{`S10`, utils.NewLocation(0, 0), utils.NewLocation(10, -9)},
		{`W20`, utils.NewLocation(0, 0), utils.NewLocation(-10, 1)},
		{`R90`, utils.NewLocation(0, 0), utils.NewLocation(1, -10)},
		{`R180`, utils.NewLocation(0, 0), utils.NewLocation(-10, -1)},
		{"R90\nR90", utils.NewLocation(0, 0), utils.NewLocation(-10, -1)},
		{`R270`, utils.NewLocation(0, 0), utils.NewLocation(-1, 10)},
		{"R90\nR90\nR90", utils.NewLocation(0, 0), utils.NewLocation(-1, 10)},
		{"R90\nR180", utils.NewLocation(0, 0), utils.NewLocation(-1, 10)},
		{"R90\nR270", utils.NewLocation(0, 0), utils.NewLocation(10, 1)},
		{"R180\nR90", utils.NewLocation(0, 0), utils.NewLocation(-1, 10)},
		{`L90`, utils.NewLocation(0, 0), utils.NewLocation(-1, 10)},
		{`L180`, utils.NewLocation(0, 0), utils.NewLocation(-10, -1)},
		{"L90\nL90", utils.NewLocation(0, 0), utils.NewLocation(-10, -1)},
		{`L270`, utils.NewLocation(0, 0), utils.NewLocation(1, -10)},
		{"L90\nL90\nL90", utils.NewLocation(0, 0), utils.NewLocation(1, -10)},
		{"L90\nL270", utils.NewLocation(0, 0), utils.NewLocation(10, 1)},
	}

	for _, test := range tests {
		ship := day12.NewShip()
		ship.AdvanceWithWaypoint(test.in)
		actualLoc := ship.CurrentLocation()
		actualWaypoint := ship.WaypointLocation()
		if !actualLoc.Equals(test.loc) || !actualWaypoint.Equals(test.waypoint) {
			t.Errorf("Advance(%q) => (loc: %v, wp: %v), want (%s, %s)",
				test.in, actualLoc, actualWaypoint, test.loc, test.waypoint)
		}
	}
}
