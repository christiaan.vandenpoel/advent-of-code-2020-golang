package day15_test

import (
	"testing"

	"advent-of-code-2020/pkg/day15"
)

func TestRambunctiousRecitation2020thNumber(t *testing.T) {
	tests := []struct {
		in    string
		count int
		out   int
	}{
		{"0,3,6", 1, 0},
		{"0,3,6", 2, 3},
		{"0,3,6", 3, 6},
		{"0,3,6", 4, 0},
		{"0,3,6", 5, 3},
		{"0,3,6", 6, 3},
		{"0,3,6", 7, 1},
		{"0,3,6", 8, 0},
		{"0,3,6", 9, 4},
		{"0,3,6", 10, 0},
		{"0,3,6", 2020, 436},
		{"1,3,2", 2020, 1},
		{"2,1,3", 2020, 10},
		{"1,2,3", 2020, 27},
		{"2,3,1", 2020, 78},
		{"3,2,1", 2020, 438},
		{"3,1,2", 2020, 1836},
	}

	for _, test := range tests {
		actual := day15.RambunctiousRecitationNthNumber(test.in, test.count)
		if actual != test.out {
			t.Errorf("RambunctiousRecitationNthNumber(%q, %d) => %d, want %d", test.in, test.count, actual, test.out)
		}
	}
}

func TestPart2(t *testing.T) {
	tests := []struct {
		in    string
		count int
		out   int
	}{
		{"0,3,6", 30000000, 175594},
		{"1,3,2", 30000000, 2578},
		{"2,1,3", 30000000, 3544142},
		{"1,2,3", 30000000, 261214},
		{"2,3,1", 30000000, 6895259},
		{"3,2,1", 30000000, 18},
		{"3,1,2", 30000000, 362},
	}

	for _, test := range tests {
		actual := day15.RambunctiousRecitationNthNumber(test.in, test.count)
		if actual != test.out {
			t.Errorf("RambunctiousRecitationNthNumber(%q, %d) => %d, want %d", test.in, test.count, actual, test.out)
		}
	}
}
