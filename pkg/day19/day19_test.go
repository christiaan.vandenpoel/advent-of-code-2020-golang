package day19_test

import (
	"testing"

	"advent-of-code-2020/pkg/day19"
)

func TestX(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"", 0},
	}

	for _, test := range tests {
		actual := day19.X(test.in)
		if actual != test.out {
			t.Errorf("X(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestY(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"", 0},
	}

	for _, test := range tests {
		actual := day19.Y(test.in)
		if actual != test.out {
			t.Errorf("Y(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
