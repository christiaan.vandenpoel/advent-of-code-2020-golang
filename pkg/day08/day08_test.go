package day08_test

import (
	"testing"

	"advent-of-code-2020/pkg/day08"
)

func TestHandheldHaltingAccumulateUniqueOps(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{`nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6`, 5},
	}

	for _, test := range tests {
		actual := day08.HandheldHaltingAccumulateUniqueOps(test.in)
		if actual != test.out {
			t.Errorf("HandheldHaltingAccumulateUniqueOps(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestHandheldHaltingRunUntilEnd(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{`nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6`, 8},
	}

	for _, test := range tests {
		actual := day08.HandheldHaltingRunUntilEnd(test.in)
		if actual != test.out {
			t.Errorf("HandheldHaltingRunUntilEnd(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
