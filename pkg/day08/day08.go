package day08

import (
	"advent-of-code-2020/pkg/utils"
	"strings"
)

func HandheldHaltingAccumulateUniqueOps(input string) int {
	program := NewProgram(input)
	return program.RunUniqueInstructionsOnly()
}

func HandheldHaltingRunUntilEnd(input string) int {
	program := NewProgram(input)
	return program.RunUntilEnd()
}

type instruction struct {
	operator string
	operand  int
}

type program struct {
	instructions []instruction
}

func NewProgram(input string) *program {
	lines := strings.Split(input, "\n")

	var program program

	for _, line := range lines {
		parts := strings.Split(line, " ")
		operator := parts[0]
		operand := utils.MustInt(parts[1])

		program.instructions = append(program.instructions, instruction{operand: operand, operator: operator})
	}
	return &program
}

func (p *program) RunUniqueInstructionsOnly() int {
	accumulator := 0

	addressVisited := make(map[int]bool)
	address := 0

	for !addressVisited[address] {
		addressVisited[address] = true
		switch p.instructions[address].operator {
		case "nop":
			address++
		case "acc":
			accumulator += p.instructions[address].operand
			address++
		case "jmp":
			address += p.instructions[address].operand
		}
	}

	return accumulator
}

func (p *program) RunUntilEnd() int {

	for idx, instruction := range p.instructions {
		var originalOperand string
		switch instruction.operator {
		case "nop":
			originalOperand = "nop"
			p.instructions[idx].operator = "jmp"
		case "jmp":
			originalOperand = "jmp"
			p.instructions[idx].operator = "nop"
		default:
			continue
		}
		acc, ok := p.SingleRunUntilEnd()
		p.instructions[idx].operator = originalOperand
		if ok {
			return acc
		}

	}

	return 0
}

func (p *program) SingleRunUntilEnd() (int, bool) {
	accumulator := 0

	addressVisited := make(map[int]bool)
	address := 0

	for !addressVisited[address] && address < len(p.instructions) {
		addressVisited[address] = true
		switch p.instructions[address].operator {
		case "nop":
			address++
		case "acc":
			accumulator += p.instructions[address].operand
			address++
		case "jmp":
			address += p.instructions[address].operand
		}
	}
	return accumulator, address == len(p.instructions)
}
