package day07_test

import (
	"testing"

	"advent-of-code-2020/pkg/day07"
)

func TestHandyHaversacksBagsForShinyGold(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{`light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.`, 4},
	}

	for _, test := range tests {
		actual := day07.HandyHaversacksBagsForShinyGold(test.in)
		if actual != test.out {
			t.Errorf("HandyHaversacksBagsForShinyGold(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestHandyHaversacksCountBagsInShinyGold(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{`light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.`, 32},
		{`shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.`, 126},
	}

	for _, test := range tests {
		actual := day07.HandyHaversacksCountBagsInShinyGold(test.in)
		if actual != test.out {
			t.Errorf("HandyHaversacksCountBagsInShinyGold(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
