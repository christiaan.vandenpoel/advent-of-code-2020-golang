This repository is a collection of my solutions for the [Advent of Code 2020](http://adventofcode.com/2020) calendar.

Puzzle                                                    |
--------------------------------------------------------- |
[Day 1: Report Repair](pkg/day01)                         |
[Day 2: Password Philosophy](pkg/day02)                   |
[Day 3: Toboggan Trajectory](pkg/day03)                   |
[Day 4: Passport Processing](pkg/day04)                   |
[Day 5: Binary Boarding](pkg/day05)                       |
[Day 6: Custom Customs](pkg/day06)                        |
[Day 7: Handy Haversacks](pkg/day07)                      |    
[Day 8: Handheld Halting](pkg/day08)                      |
[Day 9: Encoding Error](pkg/day09)                        |
[Day 10: Adapter Array](pkg/day10)                        |
[Day 11: Seating System](pkg/day11)                       |
[Day 12: Rain Risk](pkg/day12)                            |
[Day 13: Shuttle Search](pkg/day13)                       |
[Day 14: Docking Data](pkg/day14)                         |
[Day 15: Rambunctious Recitation](pkg/day15)              |
[Day 16: Ticket Translation](pkg/day16)                   |
[Day 17: Conway Cubes](pkg/day17)                         |
[Day 18: Operation Order](pkg/day18)                      |
[Day 19: Monster Messages](pkg/day19)                     |
[Day 20: Jurassic Jigsaw](pkg/day20)                      |
[Day 21: Allergen Assessment](pkg/day21)                  |
[Day 22: Crab Combat](pkg/day22)                          |
[Day 23: Crab Cups](pkg/day23)                            |
[Day 24: ](pkg/day24)                                     |
[Day 25: ](pkg/day25)                                     |
